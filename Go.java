import javax.swing.JOptionPane;

public class Go {
    
    public static void main(String[]args){

        Tablero go1 = new Tablero(9,9,2);

        
        go1.setTableroVacio();
        
        

        int opcion = -1;

        while(opcion != 0){

            opcion = Integer.parseInt(JOptionPane.showInputDialog(null, "**************Ingrese la opcion que desee************" + 
                                                                         "\n1.Crear jugadores" +
                                                                         "\n2.Jugar" + 
                                                                         "\n0.Salir" +
                                                                         "\n*****************************************************"));

            switch(opcion){

                case 1:

                    Jugador player;

                    for(int indice = 0; indice < 2; indice++){

                        String nombre = JOptionPane.showInputDialog(null, "Ingrese el nombre del jugador " + (indice + 1));
                        String color = "";
                        
						if(indice == 0){
							color = "Blanco";
							}else if(indice == 1){
								color = "Negro";
								}

                        player = new Jugador(nombre, color);

                        go1.setJugador(player, indice);
                    }

                    break;

                case 2: 

                    Ficha ficha1;
                    Ficha ficha2;

                    int opcion1 = -1;
                    
                    while(opcion1 != 0){

                        JOptionPane.showMessageDialog(null, go1.toString());

                        opcion1 = Integer.parseInt(JOptionPane.showInputDialog(null, "**************" + go1.getJugador(0).getNombre() + "  que desea hacer?************" + 
                                                                                    "\n1.Colocar una pieza en el tablero" +
                                                                                    "\n2.Pasar el turno" + 
                                                                                    "\n0.Salir" +
                                                                                    "\n****************************************************"));
                        
                        switch (opcion1) {
                            case 1:

                                int fila = Integer.parseInt(JOptionPane.showInputDialog(null,"Digite la fila"));
                                int columna = Integer.parseInt(JOptionPane.showInputDialog(null,"Digite la columna"));
                               
                                
                                ficha1 = new Ficha(go1.getJugador(0).getColor());
                                go1.setFicha(ficha1, fila, columna);
                                
                                JOptionPane.showMessageDialog(null,"La cantidad de libertades es de: " + go1.getLibertadesFicha(fila, columna));
                                
                                JOptionPane.showMessageDialog(null, go1.toString());

                                int opcion2 = Integer.parseInt(JOptionPane.showInputDialog(null, "**************" + go1.getJugador(1).getNombre() + "  que desea hacer?************" + 
                                                                                                    "\n1.Colocar una pieza en el tablero" +
                                                                                                    "\n2.Pasar el turno" + 
                                                                                                    "\n****************************************************"));

                                    if(opcion2 == 1){

                                        int fila1 = Integer.parseInt(JOptionPane.showInputDialog(null,"Digite la fila"));
                                        int columna1 = Integer.parseInt(JOptionPane.showInputDialog(null,"Digite la columna"));
                                        
                                        
                                        ficha2 = new Ficha(go1.getJugador(1).getColor());
                                        go1.setFicha(ficha2, fila1, columna1);
                                        
                                        JOptionPane.showMessageDialog(null,"La cantidad de libertades es de: " + go1.getLibertadesFicha(fila1, columna1));
                                       

                                        break;

                                    }else if(opcion2 == 2){

                                    break;
                                    }
                            case 2:
								
								JOptionPane.showMessageDialog(null, go1.toString());
								
                                opcion2 = Integer.parseInt(JOptionPane.showInputDialog(null, "**************" + go1.getJugador(1).getNombre() + "  que desea hacer?************" + 
                                                                                                "\n1.Colocar una pieza en el tablero" +
                                                                                                "\n2.Pasar el turno" + 
                                                                                                "\n****************************************************"));

                                    if(opcion2 == 1){

                                    int fila1 = Integer.parseInt(JOptionPane.showInputDialog(null,"Digite la fila"));
                                    int columna1 = Integer.parseInt(JOptionPane.showInputDialog(null,"Digite la columna"));
                                    
                                    
                                    
                                    ficha2 = new Ficha(go1.getJugador(1).getColor());
                                    go1.setFicha(ficha2, fila1, columna1);
                                    
                                    JOptionPane.showMessageDialog(null,"La cantidad de libertades es de: " + go1.getLibertadesFicha(fila1, columna1));

                                    break;

                                    }else if(opcion2 == 2){

                                    break;
                                    }
                    }
                }

            }

        }
    }

}
