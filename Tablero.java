public class Tablero{
    
    private Ficha[][] arregloTablero;
    private Jugador[] jugadores;

    public  Tablero(int fila, int columna, int indice) {

        arregloTablero = new Ficha[fila][columna];
        jugadores = new Jugador[indice];
        
    }


    public void setFicha(Ficha ficha, int fila, int columna) {
        
        arregloTablero[fila][columna] = ficha;

    }

    public Ficha getFicha(int fila, int columna) {
        return arregloTablero[fila][columna];
    }
    

    public void setJugador(Jugador jugadores, int posicion){
        this.jugadores[posicion] = jugadores;

    }

    public Jugador getJugador(int posicion){
        return jugadores[posicion];
    }
    
    public void eliminarFicha(int fila, int columna){
		Ficha vacia = new Ficha("vacia");
		arregloTablero[fila][columna] = vacia;
		
	}
    
    
    
    public int getLibertadesFicha(int fila, int columna){
		
		int libertades = 0;
		
		int filaMenos = fila - 1;
		int columnaMenos = columna -1;
		
		int filaMas = fila + 1;
		int columnaMas = columna + 1;
	
		for(int fila1 = filaMenos; fila1 <= filaMas; fila1++){
			for(int columna1 = columnaMenos; columna1 <= columnaMas; columna1++){
			if(fila1 != -1 && columna1 != -1 && fila1 < 9 && columna1 < 9){
				if(arregloTablero[fila1][columna1].toString().equals("vacio")){
					
					libertades += 1;
					
					}
				} 
				
				}
			}
		
		return libertades;
		
		}
		
	
	public int getCantidadFichasCapturadas(String color){
		
		int cantidadFichas = 0;
		
		for(int fila = 0; fila < arregloTablero.length; fila++){
			for(int columna = 0; columna < arregloTablero[0].length; columna ++){
				
				if(arregloTablero[fila][columna].getColor().equals(color)){
					
					cantidadFichas += 1;
					
					
				}
				
			}
		}
		return cantidadFichas;
	}
	
	public int getCantidadFichasTablero(String color){
		
		int cantidadFichas = 0;
		
		for(int fila = 0; fila < arregloTablero.length; fila++){
			for(int columna = 0; columna < arregloTablero[0].length; columna ++){
			
				if(arregloTablero[fila][columna].getColor().equals(color)){
				
					cantidadFichas += 1;
				
				}
				
			}
		}
		return cantidadFichas;
		
	}
		
	public String getGanador(){
		
		int jugador1 = getCantidadFichasTablero("Blanco");
		int jugador2 = getCantidadFichasTablero("Negro");
		
		if(jugador1 > jugador2){
		
			return jugadores[0].toString();
			
		}else{
			
			return jugadores[1].toString();
				
		}
		
		}
		

    public String toString() {

        String acumulador = " ";

    for(int fila = 0;fila < arregloTablero.length; fila++ ){
      for(int columna = 0; columna < arregloTablero[fila].length; columna++){

        acumulador += arregloTablero[fila][columna] + "    ";


            }
    acumulador+="\n";

        }
    return acumulador;
    }
    

       
    public int fichaFilasLength(){
        return arregloTablero.length;

    }

    public int fichasColumnasLength(int posicion){
        return arregloTablero[posicion].length;

    }
    
    

    public int jugadoresLength() {
        return jugadores.length;
    }
    
    


    public void setTableroVacio(){

        for(int fila = 0; fila < arregloTablero.length; fila++){
            for(int columna = 0; columna < arregloTablero[fila].length; columna++){
                Ficha ficha = new Ficha("vacio");
                arregloTablero[fila][columna] = ficha;

            }
        }

    }
    
    public int getTerritorioJugador(String color){
		
		int territorioColor = 0;
		int cantidadFichas = 0;
		
		int filaMenos;
		int columnaMenos;
		int filaMas;
		int columnaMas;
	
		for(int fila = 0; fila < arregloTablero.length; fila++){
            for(int columna = 0; columna < arregloTablero[fila].length; columna++){
				
				if(arregloTablero[fila][columna].toString().equals("vacio")){
					
					filaMenos = fila - 1;
					filaMas = fila + 1;
					
					columnaMenos = columna - 1;
					columnaMas = columna + 1;
					
					for(int fila1 = filaMenos; fila1 <= filaMas; fila1++){
						for(int columna1 = columnaMenos; columna1 <= columnaMas; columna1++){
							
							if(fila1 != -1 && columna1 != -1 && fila1 < 9 && columna1 < 9){
								
								if(arregloTablero[fila1][columna1].toString().equals(color)){
									
									cantidadFichas += 0;
										
								}else if(arregloTablero[fila1][columna1].toString().equals("vacio")){
									
									cantidadFichas += 1;
									
								}
							}
						}
					}
					
					if(cantidadFichas == 0){
						
						territorioColor += 1;
						
					}
				
				}
			}
		}
		
		return territorioColor;
	}

    
    
}
