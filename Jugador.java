public class Jugador {

    private String nombre;
    private String color;
    private int cantidadFichasCap = 0;
    private int cantidadFichasTablero = 0;

    public Jugador(String nombre, String color) {

        this.nombre = nombre;
        this.color = color;
        
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    
    public String getNombre() {
        return nombre;
    }

    public String getColor(){
        return color;
    }

    public void setCantidadFichasCap(int fichasCap){
        cantidadFichasCap = fichasCap;
    }

    public int getCantidadFichasCap(){
        return cantidadFichasCap;
    }
    
    public void setCantidadFichasTablero(int fichasTablero){
		cantidadFichasTablero = fichasTablero;
		
	}
	
	public int getCantidadFichasTablero(){
		return cantidadFichasTablero;
		
	}


    public String toString() {
        
        return "El ganador es: " + "\nJugador: " + getNombre() + "\nTotal de fichas en el Tablero: " + getCantidadFichasTablero() + "\nFichas Capturadas: " + getCantidadFichasCap();
    }
}
